import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import java.time.Duration;
public class CreateAccount {

    static void createaccount() {
            System.out.println("Registration Screen Opened Successfully");
            MobileElement SignUp = BeFit360.driver.findElementById("com.robusta.befit:id/sign_up_btn");
            SignUp.click();

        if(BeFit360.driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup").isDisplayed()) {
            System.out.println("Sign Up Screen Opened Successfully");
            MobileElement Name = BeFit360.driver.findElementById("com.robusta.befit:id/name_et");
            Name.sendKeys("Mohamed Ashraf");
            MobileElement Email = BeFit360.driver.findElementById("com.robusta.befit:id/email_et");
            Email.sendKeys("test.robusta15@gmail.com");
            MobileElement Password = BeFit360.driver.findElementById("com.robusta.befit:id/password_et");
            Password.sendKeys("123456");
            MobileElement PhoneNumber = BeFit360.driver.findElementById("com.robusta.befit:id/phone_number_et");
            PhoneNumber.sendKeys("+201092872783");
            MobileElement DateOfBirth = BeFit360.driver.findElementById("com.robusta.befit:id/age_et");
            DateOfBirth.click();
            /*MobileElement Year = BeFit360.driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.NumberPicker[3]/android.widget.EditText");
            Year.clear();
            Year.sendKeys("1996");
            MobileElement Day = BeFit360.driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.NumberPicker[2]/android.widget.EditText");
            Day.clear();
            Day.sendKeys("15");
            MobileElement Month = BeFit360.driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.NumberPicker[1]/android.widget.EditText");
            Month.clear();
            Month.sendKeys("Sep");*/
            MobileElement Ok = BeFit360.driver.findElementById("android:id/button1");
            Ok.click();
            MobileElement Address = BeFit360.driver.findElementById("com.robusta.befit:id/address_et");
            Address.sendKeys("1019 Baron St");
            //Scrolling
            TouchAction action = new TouchAction(BeFit360.driver);
            Dimension ScreenDimension = BeFit360.driver.manage().window().getSize();
            int XStart = (int) (ScreenDimension.width * .5);
            int YStart = (int) (ScreenDimension.height * .8);
            int XEnd = (int) (ScreenDimension.width * .5);
            int YEnd = (int) (ScreenDimension.height * .2);
            action.press(PointOption.point(XStart, YStart)).waitAction(WaitOptions.waitOptions(Duration.ofSeconds(1))).moveTo(PointOption.point(XEnd, YEnd)).release().perform();
            MobileElement Signup = BeFit360.driver.findElementById("com.robusta.befit:id/sign_up_btn");
            Signup.click();
            if(BeFit360.driver.findElementById("com.robusta.befit:id/iv_pop_up_icon").isDisplayed()) {
                MobileElement ok = BeFit360.driver.findElementById("com.robusta.befit:id/btn_popup_done");
                ok.click();
            }
            if(BeFit360.driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup").isDisplayed()) {
                MobileElement SkipPhoto = BeFit360.driver.findElementById("com.robusta.befit:id/tv_skip");
                SkipPhoto.click();
            }

            }
    }
}

