import io.appium.java_client.android.Activity;

public class LaunchNewApp {
    public LaunchNewApp(String appPackage, String appActivity) {
        BeFit360.driver.startActivity(new Activity(appPackage,appActivity));

    }
}
