import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import java.net.MalformedURLException;

public class BeFit360 extends Dependencies{
    public static AndroidDriver<AndroidElement> driver;

    static {
        try {
            driver = dependencies();
            System.out.println("Server connected successfully");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            System.out.println("Server failed to connect");
        }
    }

    public static void main(String[] args) throws InterruptedException {
    Splash.splash();
    CreateAccount.createaccount();
    new LaunchNewApp("com.google.android.gm","com.google.android.gm.ConversationListActivityGmail");
    MailConfirmation.mailconfirmation();
    LogIn.login();

    }
}
