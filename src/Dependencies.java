import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import static io.appium.java_client.remote.MobileCapabilityType.DEVICE_NAME;



    public class Dependencies {

        public static AndroidDriver<AndroidElement> dependencies() throws MalformedURLException {

            DesiredCapabilities dc = new DesiredCapabilities();
            AndroidDriver<AndroidElement> driver;
            dc.setCapability(DEVICE_NAME, "16d024470205");
            dc.setCapability("platformName", "android");
            dc.setCapability("appPackage", "com.robusta.befit");
            dc.setCapability("appActivity", "com.robusta.befit.features.splash.SplashActivity");
            driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), dc);
            driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

            return driver;

        }


    }

