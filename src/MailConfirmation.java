import com.google.common.collect.ImmutableMap;
import io.appium.java_client.MobileElement;

import java.util.concurrent.TimeUnit;


public class MailConfirmation {
    static void mailconfirmation() throws InterruptedException {

        MobileElement SearchBar = BeFit360.driver.findElementById("com.google.android.gm:id/open_search");
        SearchBar.click();
        MobileElement SearchFor = BeFit360.driver.findElementById("com.google.android.gm:id/open_search_view_edit_text");
        SearchFor.sendKeys("Befit");
        BeFit360.driver.executeScript("mobile: performEditorAction", ImmutableMap.of("action", "search"));
        MobileElement ConfirmMail = BeFit360.driver.findElementById("com.google.android.gm:id/viewified_conversation_item_view");
            if(ConfirmMail.getAttribute("text").contains("Verify Email Address")){
                ConfirmMail.click();
            }
        MobileElement Verify = BeFit360.driver.findElementByAccessibilityId("Verify Email Address");
        Verify.click();
        TimeUnit.SECONDS.sleep(2);
        do {
        BeFit360.driver.navigate().back();
        }
        while (BeFit360.driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.view.ViewGroup").isDisplayed()==false);
    }
}
