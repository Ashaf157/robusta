import io.appium.java_client.MobileElement;

public class Splash {
    static void splash() {

        if(BeFit360.driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup").isDisplayed()){
            System.out.println("Splash Screen Opened Successfully");
            MobileElement SplashSkip = BeFit360.driver.findElementById("com.robusta.befit:id/skip_done_tv");
            SplashSkip.click();
        }
    }
}
